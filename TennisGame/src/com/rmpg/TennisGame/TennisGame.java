package com.rmpg.TennisGame;

public class TennisGame
{
    private int serverPointToken = 0;
    private int secondPlayerPointToken = 0;
    private boolean isDuece = false;
    private final String player1 = "Bjorn Borg";
    private final String player2 = "John McEnroe";

    public String getScore()
    {
        if (isDuece)
            return whoHasAdvantage();
        else
            return getCurrentScore();
    }

    public void wonPoint(String player)
    {
        if (player.equals(player1))
            serverPointToken++;
        if (player.equals(player2))
            secondPlayerPointToken++;
        if (!isDuece)
            isADuece();
    }

    private String whoHasAdvantage()
    {
        if (serverPointToken == secondPlayerPointToken)
            return "Duece";
        else if (serverPointToken == secondPlayerPointToken + 1)
            return "Advantage " + player1;
        else if (secondPlayerPointToken == serverPointToken + 1)
            return "Advantage " + player2;
        else if (serverPointToken == secondPlayerPointToken + 2)
            return "Win for " + player1;
        return "Win for " + player2;

    }

    private String getCurrentScore()
    {
        if (serverPointToken == 4)
            return "Win for " + player1;
        else if (secondPlayerPointToken == 4)
            return "Win for " + player2;
        return getString(serverPointToken) + " " + getString(secondPlayerPointToken);
    }

    private String getString(int point)
    {
        switch (point)
        {
            case 1:
                return "Fifteen";
            case 2:
                return "Thirty";
            case 3:
                return "Forty";
            default:
                return "Love";
        }

    }

    private void isADuece()
    {
        isDuece = ((serverPointToken == 3 && secondPlayerPointToken == 3));
    }
}
