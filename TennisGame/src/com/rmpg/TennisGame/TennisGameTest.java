package com.rmpg.TennisGame;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TennisGameTest
{
    TennisGame tg = new TennisGame();
    private final String player1 = "Bjorn Borg";
    private final String player2 = "John McEnroe";

    @Test
    public void getLovetest()
    {
        assertEquals("Love Love", tg.getScore());
    }

    @Test
    public void serverGetsAPointTest()
    {
        tg.wonPoint(player1);
        assertEquals("Fifteen Love", tg.getScore());
    }

    @Test
    public void serverGetsAnotherPointTest()
    {
        tg.wonPoint(player1);
        tg.wonPoint(player1);
        assertEquals("Thirty Love", tg.getScore());
    }

    @Test
    public void secondPlayerGetsFirstPointTest()
    {
        tg.wonPoint(player2);
        assertEquals("Love Fifteen", tg.getScore());
    }

    @Test
    public void dueceTest()
    {
        tg.wonPoint(player1);
        tg.wonPoint(player1);
        tg.wonPoint(player2);
        tg.wonPoint(player2);
        tg.wonPoint(player1);
        tg.wonPoint(player2);
        assertEquals("Duece", tg.getScore());
    }

    @Test
    public void secondHasAdvantage()
    {
        tg.wonPoint(player1);
        tg.wonPoint(player1);
        tg.wonPoint(player1);
        tg.wonPoint(player2);
        tg.wonPoint(player2);
        tg.wonPoint(player2);
        tg.wonPoint(player2);
        assertEquals("Advantage " + player2, tg.getScore());
    }

    @Test
    public void serverWinsTest()
    {
        tg.wonPoint(player1);
        tg.wonPoint(player1);
        tg.wonPoint(player1);
        tg.wonPoint(player1);
        assertEquals("Win for " + player1, tg.getScore());
    }

    @Test
    public void secondWinsAfterDuece()
    {
        tg.wonPoint(player1);
        tg.wonPoint(player1);
        tg.wonPoint(player1);
        tg.wonPoint(player2);
        tg.wonPoint(player2);
        tg.wonPoint(player2);
        tg.wonPoint(player2);
        tg.wonPoint(player2);
        assertEquals("Win for " + player2, tg.getScore());
    }
}
