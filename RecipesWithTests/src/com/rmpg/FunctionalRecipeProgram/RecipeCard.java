package com.rmpg.FunctionalRecipeProgram;

import java.util.List;

public class RecipeCard
{
    String name;
    String typeOfMeal;
    String ethnicity;
    List<String> ingredients;
    List<String> directions;

    public static class Builder
    {
        private String name;
        private String typeOfMeal;
        private String ethnicity;
        private List<String> ingredients;
        private List<String> directions;

        public Builder()
        {
            return;
        }

        public Builder name(String name)
        {
            this.name = name;
            return this;
        }

        public Builder typeOfMeal(String type)
        {
            this.typeOfMeal = type;
            return this;
        }

        public Builder ethnicity(String eth)
        {
            this.ethnicity = eth;
            return this;
        }

        public Builder ingredients(List<String> ing)
        {
            this.ingredients.addAll(ing);
            return this;
        }

        public Builder directions(List<String> dir)
        {
            this.directions.addAll(dir);
            return this;
        }

        public RecipeCard build()
        {
            return new RecipeCard(this);
        }
    }

    private RecipeCard(Builder rc)
    {
        this.name = rc.name;
        this.typeOfMeal = rc.typeOfMeal;
        this.ethnicity = rc.ethnicity;
        this.ingredients = rc.ingredients;
        this.directions = rc.directions;
    }

}
