package com.rmpg.FunctionalRecipeProgram;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RecipeOrganizer
{
    private List<RecipeCard> recipeStack = new ArrayList<>();
    static final String RECIPE_DELIM = "***>>><<<***";
    final static String DEFAULT_RECIPE_FILE = "c:\\JavaSchoolWork\\formattedRecipeString.txt";

    public RecipeOrganizer()
    {

    }

    public RecipeOrganizer(String recipeStr)
    {
        recipeStack = makeCookbook(recipeStr);
    }

    public List<RecipeCard> getCookbook()
    {
        return recipeStack;
    }

    public void setCookbook(List<RecipeCard> input)
    {
        recipeStack.clear();
        recipeStack.addAll(input);
    }

    protected List<RecipeCard> specifyCookbook(String type)
    {
        List<RecipeCard> specifiedRecipes = new ArrayList<>();
        for (RecipeCard next : recipeStack)
            if (next.typeOfMeal.equals(type))
                specifiedRecipes.add(next);
        return specifiedRecipes;
    }

    private List<RecipeCard> makeCookbook(String strOfRecipes)
    {
        List<RecipeCard> cookbook = new ArrayList<>();
        while (strOfRecipes.contains(RECIPE_DELIM))
        {
            ParseStringAndFile str = new ParseStringAndFile();
            String nextRecipeStr = strOfRecipes.substring(0, strOfRecipes.indexOf(RECIPE_DELIM)).trim();
            RecipeCard nextRecipe = new RecipeCard(nextRecipeStr);
            strOfRecipes = str.parseString(strOfRecipes, RECIPE_DELIM);
            cookbook.add(nextRecipe);
        }
        return cookbook;
    }

    protected List<RecipeCard> randomizeCookbook() // how to make this private?
    {
        List<RecipeCard> randomizedCookbook = new ArrayList<>();
        List<RecipeCard> cookbookOfRemovedEthnicities = new ArrayList<>();
        Random randNum = new Random();

        while (!recipeStack.isEmpty())
        {
            if (cookbookOfRemovedEthnicities.isEmpty())
                cookbookOfRemovedEthnicities.addAll(recipeStack);
            RecipeCard nextRecipe = new RecipeCard();
            nextRecipe = (cookbookOfRemovedEthnicities.get(randNum.nextInt(cookbookOfRemovedEthnicities.size())));
            randomizedCookbook.add(nextRecipe);
            for (int i = 0; i < cookbookOfRemovedEthnicities.size(); i++)
            {
                if (cookbookOfRemovedEthnicities.get(i).ethnicity.equals(nextRecipe.ethnicity))
                {
                    cookbookOfRemovedEthnicities.remove(i);
                    i--;
                }
            }
            recipeStack.remove(nextRecipe);
        }

        return randomizedCookbook;
    }

}
