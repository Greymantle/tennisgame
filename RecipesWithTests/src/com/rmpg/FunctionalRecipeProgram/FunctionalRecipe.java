package com.rmpg.FunctionalRecipeProgram;

public class FunctionalRecipe
{
    final static String RECIPE_FILE = "c:\\JavaSchoolWork\\formattedRecipeString.txt";
    ParseStringAndFile fileToParse = new ParseStringAndFile();
    String strFromFile = fileToParse.getStrFromFile(RECIPE_FILE);
    final static String MAIN_DISH = "Main Dish";
    final static String SIDE_DISH = "Side Dish";

    public FunctionalRecipe()
    {

    }

    public static void main(String[] args)
    {
        FunctionalRecipe program = new FunctionalRecipe();
        if (args.length == 1)
            try
            {
                program.runLimitedNumRandRecipeWithSameEthSide(Integer.parseInt(args[0]));
            }
            catch (NumberFormatException e)
            {
                System.out.println("Must be a number");
            }
    }

    public void runLimitedNumRandRecipeWithSameEthSide(int args)
    {
        RecipeOrganizer mainDishCookbook = new RecipeOrganizer(strFromFile);
        mainDishCookbook.setCookbook(mainDishCookbook.specifyCookbook(MAIN_DISH));
        mainDishCookbook.setCookbook(mainDishCookbook.randomizeCookbook());

        RecipeOrganizer sideDishCookbook = new RecipeOrganizer(strFromFile);
        sideDishCookbook.setCookbook(sideDishCookbook.specifyCookbook(SIDE_DISH));
        sideDishCookbook.setCookbook(sideDishCookbook.randomizeCookbook());
        
        for (int i = 0; i < args; i++)
        {
            RecipeCard side = new RecipeCard();
            for (RecipeCard next : sideDishCookbook.getCookbook())
            {
                if (next.ethnicity.equals(mainDishCookbook.getCookbook().get(i).ethnicity))
                {
                    side = next;
                    break;
                }
            }
            System.out.println(mainDishCookbook.getCookbook().get(i).name + " with " + side.name);
        }
    }
}
