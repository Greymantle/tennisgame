package com.rmpg.FunctionalRecipeProgram;

import static org.junit.Assert.*;

import org.junit.Test;

public class ParseStringAndFileTest
{

    @Test
    public void makeFileReader()
    {
        ParseStringAndFile str = new ParseStringAndFile();
        final String RECIPE_FILE = "c:\\JavaSchoolWork\\formattedRecipeString.txt";
        String strFromFile = str.getStrFromFile(RECIPE_FILE);
        assertEquals("Tacos", strFromFile.subSequence(0, 5));
    }

    @Test
    public void removeDelim()
    {
        final String DELIM = "<<**>>";
        ParseStringAndFile str = new ParseStringAndFile();
        assertEquals("Test string", str.parseString("<<**>>Test string", DELIM));
    }
}
