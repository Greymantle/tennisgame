package com.rmpg.FunctionalRecipeProgram;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

public class FunctionalRecipeTest
{
    ParseStringAndFile str = new ParseStringAndFile();
    final static String RECIPE_FILE = "c:\\JavaSchoolWork\\formattedRecipeString.txt";
    String strFromFile = str.getStrFromFile(RECIPE_FILE);
    RecipeOrganizer originalTestRecipe = new RecipeOrganizer(strFromFile);

    @Test
    public void returnARecipeName()
    {
        RecipeOrganizer tr = new RecipeOrganizer("Tacos ***>>><<<***");
        List<RecipeCard> testCookbook = tr.getCookbook();
        assertEquals("Tacos", testCookbook.get(0).name);
    }

    @Test
    public void returnFirstOfSeveralNames()
    {
        RecipeOrganizer tr = new RecipeOrganizer("Tacos ***>>><<<*** Pizza ***>>><<<***");
        List<RecipeCard> testCookbook = tr.getCookbook();
        assertEquals("Tacos", testCookbook.get(0).name);
    }

    @Test
    public void returnSecondRecipeName()
    {
        RecipeOrganizer tr = new RecipeOrganizer("Tacos ***>>><<<*** Pizza ***>>><<<***");
        List<RecipeCard> testCookbook = tr.getCookbook();
        assertEquals("Pizza", testCookbook.get(1).name);
    }

    @Test
    public void addAnEthnicityToStr()
    {
        RecipeOrganizer tr = new RecipeOrganizer("Tacos <<**>> Mexican ***>>><<<*** Pizza <<**>> Italian ***>>><<<***");
        List<RecipeCard> testCookbook = tr.getCookbook();
        assertEquals("Tacos", testCookbook.get(0).name);
    }

   

    @Test
    public void returnRecipeFromFile()
    {
        RecipeOrganizer tr = new RecipeOrganizer(strFromFile.subSequence(0, 5) + " ***>>><<<***");
        List<RecipeCard> testCookbook = tr.getCookbook();
        assertEquals("Tacos", testCookbook.get(0).name);
    }

    @Test
    public void getCookbook()
    {
        String[] recipeNameArray = { "Tacos", "Pizza", "Stir Fry", "Western Meal", "Spaghetti", "Raman", "Burritos" };
        List<String> recipeNamesExpected = new ArrayList<>();
        for (String name : recipeNameArray)
            recipeNamesExpected.add(name); // what I expect

        for (int i = 0; i < recipeNamesExpected.size(); i++)
            assertEquals(recipeNamesExpected.get(i), originalTestRecipe.getCookbook().get(i).name);
    }

    @Ignore
    // Sysout, ran so ignoring
    @Test
    public void printCookbook()
    {
        for (int i = 0; i < originalTestRecipe.getCookbook().size(); i++)
        {
            System.out.println(originalTestRecipe.getCookbook().get(i).name);
        }
    }

    @Test
    public void getEthnicity()
    {
        assertEquals("Mexican", originalTestRecipe.getCookbook().get(0).ethnicity);
    }

    @Test
    public void getListOfIngredients()
    {
        assertEquals("some other ingredient", originalTestRecipe.getCookbook().get(1).ingredients.get(1));
    }

    @Ignore
    // Sysout, ran so ignoring
    @Test
    public void randomizeCookbook()
    {
        List<RecipeCard> tr = new ArrayList<>();
        tr = originalTestRecipe.randomizeCookbook();
        for (int i = 0; i < tr.size(); i++)
        {
            System.out.println(tr.get(i).name);
        }
    }

    @Test
    public void getDirections()
    {
        {
            assertEquals("some direction", originalTestRecipe.getCookbook().get(2).directions.get(0));
        }
    }

    @Ignore
    // Sysout, ran so ignoring
    @Test
    public void numberOfMealsTest()
    {
        List<RecipeCard> tr = new ArrayList<>();
        tr = originalTestRecipe.randomizeCookbook();
        for (int i = 0; i < 5; i++)
        {
            System.out.println(tr.get(i).name);
        }
    }

    @Test
    public void eliminateSideDish()
    {
        boolean b = true;
        originalTestRecipe.setCookbook(originalTestRecipe.specifyCookbook("Side Dish"));
        for (RecipeCard next : originalTestRecipe.getCookbook())

        {
            if (next.typeOfMeal == "Side Dish")
            {
                b = false;
                break;
            }
        }
        assertTrue(b);
    }

    @Test
    public void runFromCL()
    {
        String[] testNum = { "5" };
        FunctionalRecipe.main(testNum);
    }

    @Test
    public void getSideDish()
    {

        assertEquals("Side Dish", originalTestRecipe.getCookbook().get(7).typeOfMeal);
    }

    @Test
    public void makeSideCookbook()
    {
        List<RecipeCard> testSideCookbook = originalTestRecipe.specifyCookbook("Side Dish");
        assertEquals("French Fries", testSideCookbook.get(0).name);
    }

    @Test
    public void giveMeARecipeWithASideDishTest()
    {
        List<RecipeCard> testMainCookbook = originalTestRecipe.specifyCookbook("Main Dish");
        List<RecipeCard> testSideCookbook = originalTestRecipe.specifyCookbook("Side Dish");
        assertTrue(testMainCookbook.get(1).typeOfMeal.matches("Main Dish")
            && testSideCookbook.get(1).typeOfMeal.matches("Side Dish"));
    }

    @Test
    public void ethnicitiesMatch()
    {
        RecipeCard mainDish = new RecipeCard();
        RecipeCard sideDish = new RecipeCard();

        assertTrue(mainDish.ethnicity == sideDish.ethnicity);
    }
}
