package com.rmpg.FunctionalRecipeProgram;

import java.io.FileReader;
import java.io.IOException;

public class ParseStringAndFile
{
    String parseString(String strToParse, String delim)
    {
            strToParse = strToParse.substring(strToParse.indexOf(delim));
            strToParse = strToParse.replaceFirst("\\Q" + delim + "\\E", " ");
            strToParse = strToParse.trim();
            return strToParse;
        
    }

    String getStrFromFile(String fileName)
    {
        String charsToStr = "";
        try
        {
            final FileReader recipeReader = new FileReader(fileName);
            int nextChar = 0;
            while ((nextChar = recipeReader.read()) != -1)
                charsToStr = charsToStr + (char) nextChar;
            recipeReader.close();
        }
        catch (IOException e)
        {
            System.out.println("File not found.");
        }
        return charsToStr;
    }
}
