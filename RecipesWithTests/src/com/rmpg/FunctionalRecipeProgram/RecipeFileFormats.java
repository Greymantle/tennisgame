package com.rmpg.FunctionalRecipeProgram;

import java.util.ArrayList;
import java.util.List;

import com.rmpg.FunctionalRecipeProgram.RecipeCard.Builder;

public class RecipeFileFormats
{
    String name;
    String typeOfMeal;
    String ethnicity;
    List<String> ingredients;
    List<String> directions;
    static final String DELIM = "<<**>>";
    static final String ING_AND_DIR_DELIM = ";;;";

    public RecipeCard makeRecipeCard(String formattedRecipeStr)
    {
        Builder rc = new RecipeCard.Builder();
        List<String> nextRecipePart = getNextRecipePart(formattedRecipeStr, DELIM);
        switch (nextRecipePart.size())
        {
            case 5:
                rc.directions(getNextRecipePart(nextRecipePart.get(4), ING_AND_DIR_DELIM));
            case 4:
                rc.ingredients(getNextRecipePart(nextRecipePart.get(3), ING_AND_DIR_DELIM));
            case 3:
                rc.equals(nextRecipePart.get(2));
            case 2:
                rc.typeOfMeal(nextRecipePart.get(1));
            case 1:
                rc.name(nextRecipePart.get(0));
        }

        return rc.build();
    }

    private List<String> getNextRecipePart(String formattedRecipeStr, String delim)
    {
        if (!formattedRecipeStr.contains(delim)) // This is just so the first tests would pass
            formattedRecipeStr = formattedRecipeStr.concat(delim);

        List<String> nextRecipePart = new ArrayList<>();

        do
        {
            ParseStringAndFile str = new ParseStringAndFile();
            String nextPartOfRecipe = formattedRecipeStr.substring(0, formattedRecipeStr.indexOf(delim));
            nextPartOfRecipe = nextPartOfRecipe.trim();
            nextRecipePart.add(nextPartOfRecipe);
            formattedRecipeStr = str.parseString(formattedRecipeStr, delim);
        } while (formattedRecipeStr.contains(delim));
        return nextRecipePart;
    }
}
