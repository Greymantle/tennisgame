package com.rmpg.recipes;

public class RecipeCard
{
	private String name;
	private boolean isItVegetarian;
	private int numberOfServings;
	private int calories;
	private double cost;
	private IngredientList recipeIngredients;
	
	public RecipeCard()
	{
	    
	}

	public RecipeCard fillOutRecipeCard(String recipeString)
	{	    
	    
	    RecipeCard nextRecipe = new RecipeCard();
	    
	    name = recipeString.substring(0,recipeString.indexOf(","));
		recipeString = recipeString.substring(recipeString.indexOf(",") + 2);

		if (recipeString.substring(0,1).equals("f"))
			isItVegetarian = false;
		else 
			isItVegetarian = true;
		recipeString = recipeString.substring(recipeString.indexOf(",") + 2);
		
		numberOfServings = Integer.parseInt(recipeString.substring(0,recipeString.indexOf(",")));
		recipeString = recipeString.substring(recipeString.indexOf(",") + 2);
			
		calories = Integer.parseInt(recipeString.substring(0, recipeString.indexOf(",")));
		recipeString = recipeString.substring(recipeString.indexOf(",") + 2);

		cost = Double.parseDouble(recipeString.substring(0, recipeString.indexOf(",")));
		recipeString = recipeString.substring(recipeString.indexOf(",") + 2);
			
		recipeIngredients = new IngredientList(recipeString);
		
		return nextRecipe;
	}
	
	public double getCost()
	{
		return cost;
	}
	
	public double getNumOfServings()
	{
		return numberOfServings;
	}

	public void print()
	{
		System.out.println(name);
		if (isItVegetarian) System.out.println(name + " is vegetarian.");
		else System.out.println(name + " is not vegetarian");
		if (numberOfServings == 1) 
			System.out.println(name + " will serve 1 person."); 
		else System.out.println(name + " will serve " + numberOfServings + " people.");
		System.out.println(name + " has " + calories + " calories.");
		System.out.println(name + " costs $" + (cost / numberOfServings) + " per serving.");
		System.out.println("The ingredients for " + name + " are ");
	
		recipeIngredients.print();
	}
}
