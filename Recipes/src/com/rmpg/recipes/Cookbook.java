package com.rmpg.recipes;

public class Cookbook
{
    RecipeCard[] cookbook;
    
    public static RecipeCard[] getBook()
    {
        RecipeCard[] cookbookToReturn = createCookBook();
        return cookbookToReturn;
    }
    
    private final static RecipeCard[] createCookBook()
    {
        RecipeCard thisRecipe = new RecipeCard();
        String ingListStr = IngredientString.getString();
        while (ingListStr.indexOf(";") != -1);
        {
            String nextRecipe = ingListStr.substring(0, ingListStr.indexOf(";"));
            ingListStr = ingListStr.substring(ingListStr.indexOf(";"));
            increaseCookbookSize(cookbook);
            cookBook[cookBook.length - 1] = thisRecipe.fillOutRecipeCard(nextRecipe);;
        }
        
        return cookBook;
    }
    
    private final static RecipeCard[] increaseCookbookSize(RecipeCard[] cookBookToBeAddedTo)
    {
        RecipeCard[] tempArray = new RecipeCard[cookBookToBeAddedTo.length + 1];
        for (int index = 0; index < cookBookToBeAddedTo.length; index++)
            tempArray[index] = cookBookToBeAddedTo[index];
        cookBookToBeAddedTo = tempArray;
        return cookBookToBeAddedTo;
    }
    
    final RecipeCard[] addToCookBook(RecipeCard[] cookBookToBeAddedTo, String someRecipe)
    {
        RecipeCard[] tempArray = new RecipeCard[cookBookToBeAddedTo.length + 1];
        for (int index = 0; index < cookBookToBeAddedTo.length; index++)
            tempArray[index] = cookBookToBeAddedTo[index];
        cookBookToBeAddedTo = tempArray;
        RecipeCard any = new RecipeCard();
        cookBookToBeAddedTo[tempArray.length - 1] = any.fillOutRecipeCard(someRecipe);
        return cookBookToBeAddedTo;
    }   
}
