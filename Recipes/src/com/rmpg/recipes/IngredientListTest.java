package com.rmpg.recipes;

import static org.junit.Assert.*;

import org.junit.Test;

public class IngredientListTest
{
    IngredientList testIng = new IngredientList();
    String ingStr = "cup of cereal, 1.0, cup of milk, 1.0";
    @Test
    public void testParseFirstIng()
    {
       assertEquals("cup of cereal",testIng.parseIngName(ingStr));
    }

}
