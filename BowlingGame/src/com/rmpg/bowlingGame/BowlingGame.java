package com.rmpg.bowlingGame;

public class BowlingGame
{
    private int frameScore = 0;
    
    public BowlingGame()
    {
        
    }
    
	final public int gameScore(int[] thrownBalls)
	{
        while (thrownBalls.length > 0)
        {
        	if (thrownBalls[0] == 10)
        	{
        	    frameScore += 10 + (thrownBalls[1] + thrownBalls[2]);
        	    if (thrownBalls.length == 3)
        	        return frameScore;
        	    thrownBalls = reduceArrayByOne(thrownBalls);
        	}
        	else if ((thrownBalls[0] + thrownBalls[1]) == 10)
        	{
        	    frameScore += thrownBalls[0] + thrownBalls[1] + thrownBalls[2];
        	    if (thrownBalls.length == 3)
                    return frameScore;
        	    thrownBalls = reduceArrayByOne(thrownBalls);
                thrownBalls = reduceArrayByOne(thrownBalls);
        	}
        	 else
        	 {
        	     frameScore += addTwoBallsToFrame(thrownBalls);
                 thrownBalls = reduceArrayByOne(thrownBalls);
                 thrownBalls = reduceArrayByOne(thrownBalls);
        	 }
        }

		return frameScore;
	}

	final private int addTwoBallsToFrame(int[] thrownBalls)
	{
	    int firstBall = thrownBalls[0];
        int secondBall = thrownBalls[1];
        return firstBall + secondBall;
    }
	
	final private int[] reduceArrayByOne(int[] importArray)
	{
		final int[] tempArray = new int[(importArray.length - 1)];
    	for (int i = 0; i < (importArray.length - 1); i++)
    		tempArray[i] = importArray[i + 1];
    	return tempArray;
	}
}
